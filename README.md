# README #

Java Header Updater updates java headers from a file containing the required header.

### Usage ###

To update all java files in a directory using a license file as a header, run:
```
#!

$ java -jar java-header-updater-1.0.4.jar directory LICENSE.txt
```

NOTE: don't include end separator in the directory path

### Latest version ###

 - **1.0.4** Added code coverage and updated build configuration

* [Download](https://bitbucket.org/borisredkin/java-header-updater/downloads/java-header-updater-1.0.4.jar)

### Old versions ###
 - **1.0.3** Updated dependencies of the project
 
 - **1.0.2** Added [VersionEye](https://www.versioneye.com/) and SonarQube to the project

 - **1.0.1** Single asterisk in copyright header

 - **1.0.0** Initial version

### Who do I talk to? ###

[![](https://stackexchange.com/users/flair/4007082.png)](https://stackoverflow.com/users/story/3301492)


[![Dependency Status](https://www.versioneye.com/user/projects/5a72fbfb0fb24f5edd750b1f/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5a72fbfb0fb24f5edd750b1f)